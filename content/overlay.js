/*
 *  Overlay Javascript for Thunderbird
 *
 *  Copyright (c) 2009 Patrik Dufresne & Ruben Verweij
 *  Copyright (c) 2010,2014 Steven Eastland
 */

/*
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 */


/**
 * Reference to the interface defined in nsIMessengerNotifications.idl
 */
const nsIMessengerNotifications = Components.interfaces.nsIMessengerNotifications;

/**
 * Reference to the required base interface that all components must support
 */
const nsISupports = Components.interfaces.nsISupports;
const nsIComponentRegistrar = Components.interfaces.nsIComponentRegistrar;
const nsICategoryManager = Components.interfaces.nsICategoryManager;
const nsIStringBundleService = Components.interfaces.nsIStringBundleService;
const nsIMsgFolderListener = Components.interfaces.nsIMsgFolderListener;
const nsIMsgFolderNotificationService = Components.interfaces.nsIMsgFolderNotificationService;

/**
 * UUID uniquely identifying our component
 * You can get from: http://kruithof.xs4all.nl/uuid/uuidgen here
 */
const CLASS_ID = Components.ID("{4157dfb0-173f-11de-8c30-0800200c9a66}");

/**
 * Description
 */
const CLASS_NAME = "Messenger Notification Javascript XPCOM Component";

/**
 * Textual unique identifier
 */
const CONTRACT_ID = "@fdonotify.info/messenger-notifications;1";

const THUNDERBIRD_ID = "{3550f703-e582-4d05-9a08-453d09bdfdc6}";

const BUNDLE_LOCATION = "chrome://fdonotify/locale/fdonotify.properties";

const BUNDLE_BRAND_LOCATION = "chrome://branding/locale/brand.properties";

// There are namespace conflicts so all constants are prepended with FDONOTIFY

const FDONOTIFY_MSG_FLAG_NEW = 0x10000;
const FDONOTIFY_FOLDER_FLAG_INBOX = 0x1000;
const FDONOTIFY_FOLDER_FLAG_TRASH = 0x0100;
const FDONOTIFY_FOLDER_FLAG_DRAFTS = 0x0400;
const FDONOTIFY_FOLDER_FLAG_TEMPLATES = 0x400000;
const FDONOTIFY_FOLDER_FLAG_JUNK = 0x40000000;
const FDONOTIFY_FOLDER_FLAG_NEWSGROUP = 0x0001;
const FDONOTIFY_FOLDER_FLAG_NEWS_HOST = 0x0001;
const FDONOTIFY_FOLDER_FLAG_QUEUE =  0x0800;
const FDONOTIFY_FOLDER_FLAG_SENTMAIL = 0x0200;
const FDONOTIFY_FOLDER_FLAG_IMAP_NOSELECT = 0x1000000;
const FDONOTIFY_FOLDER_FLAG_CHECK_NEW = 0x20000000;

/**
 * Class constructor
 */
function MessengerNotifications() {
	this.init();
};

/**
 * Class definition
 */
MessengerNotifications.prototype = {

	logger: function logger(msg) {
		/*
		var Application = Components.classes["@mozilla.org/steel/application;1"].getService(Components.interfaces.steelIApplication);
		Application.console.log("[fdonotify]: " + msg);
		*/
	},

	addonCallback: function(addon) {
		// Get path to notify.py script
		fdonotify.mNotifyPath = addon.getResourceURI("notify-dbus.py").QueryInterface(Components.interfaces.nsIFileURL).file.path;
		fdonotify.finishInit();
	},

	init: function() {
		this.logger("MessengerNotifications::init");
		Components.utils.import("resource://gre/modules/AddonManager.jsm");
		AddonManager.getAddonByID("fdonotify@steven.eastland", this.addonCallback);
	},

	finishInit: function() {
		this.logger("MessengerNotifications::finishInit");

		// If you only need to access your component from Javascript, uncomment the
		// following line:
		this.wrappedJSObject = this;

		// Retrieve String Bundles
		var sbs = Components.classes["@mozilla.org/intl/stringbundle;1"]
		                    .getService(nsIStringBundleService);
		this.mBundle = sbs.createBundle(BUNDLE_LOCATION);
		this.mBundleBrand = sbs.createBundle(BUNDLE_BRAND_LOCATION);

		// Initialize array for notification queue
		this.mailQueue = { folderList: [] };

		// Registering listener
		var notificationService = Components.classes["@mozilla.org/messenger/msgnotificationservice;1"]
		                                    .getService(nsIMsgFolderNotificationService);
		notificationService.addListener(this, notificationService.msgAdded);
    
		// Check if fdonotify.oneNotification exists
		// Check if fdonotify.showFolder exists
		// Check if fdonotify.showNews exists
		// Check if fdonotify.persistentNotifications exists
		var prefs = Components.classes["@mozilla.org/preferences-service;1"]
		                      .getService(Components.interfaces.nsIPrefBranch);
		if (!prefs.prefHasUserValue("fdonotify.oneNotification")) {
			prefs.setBoolPref("fdonotify.oneNotification", false);
		}
		if (!prefs.prefHasUserValue("fdonotify.showFolder")) {
			prefs.setBoolPref("fdonotify.showFolder", false);
		}
		if (!prefs.prefHasUserValue("fdonotify.showNews")) {
			prefs.setBoolPref("fdonotify.showNews", false);
		}
		if (!prefs.prefHasUserValue("fdonotify.persistentNotifications")) {
			prefs.setBoolPref("fdonotify.persistentNotifications", true);
		}

		this.mNotifyTimer = null;
	},

  //////////////////////////////////////////////////////////////////////////////
  //// nsIMsgFolderListener
  /**
   * Receive an event when a new mail is added to a folder.
   */
	msgAdded: function msgAdded(aMsg) { // TB 3
		if (aMsg instanceof Components.interfaces.nsIMsgDBHdr) {
			var header = aMsg.QueryInterface(Components.interfaces.nsIMsgDBHdr);
			var folder = header.folder.QueryInterface(Components.interfaces.nsIMsgFolder);
			if (this.checkFolder(folder) && (header.flags & FDONOTIFY_MSG_FLAG_NEW)) {
				var folderName = folder.prettiestName;
				var rootFolderName = folder.rootFolder.prettiestName;
				var subject = header.mime2DecodedSubject
				var author = header.mime2DecodedAuthor;
				this.handleNewMailReceive(rootFolderName, folderName, subject, author);
			}
		}
	},

  /**
   * This determines if this folder should even be checked to send a
   * notification to D-Bus.
   */
	checkFolder: function checkFolder(aFolder) {
		this.logger("MessengerNotifications::checkFolder");

		var prefs = Components.classes["@mozilla.org/preferences-service;1"]
		                      .getService(Components.interfaces.nsIPrefBranch);
		var showNews = prefs.getBoolPref("fdonotify.showNews");

		// if show newsgroups is turned off
		if (!showNews && (aFolder.flags & (FDONOTIFY_FOLDER_FLAG_NEWSGROUP | FDONOTIFY_FOLDER_FLAG_NEWS_HOST))) {
			return false;
		}

		// If it's the Inbox let it through without checking other flags
		if (aFolder.flags & FDONOTIFY_FOLDER_FLAG_INBOX) {
			return true;
		}

		// Ignore certain folders because they don't contain useful stuff
		if (aFolder.flags & (FDONOTIFY_FOLDER_FLAG_TRASH |
		                      FDONOTIFY_FOLDER_FLAG_JUNK |
		                      FDONOTIFY_FOLDER_FLAG_SENTMAIL |
		                      FDONOTIFY_FOLDER_FLAG_DRAFTS |
		                      FDONOTIFY_FOLDER_FLAG_TEMPLATES |
		                      FDONOTIFY_FOLDER_FLAG_QUEUE |
													FDONOTIFY_FOLDER_FLAG_IMAP_NOSELECT)) {
			return false;
		}

		// If it's explicitly marked check new
		if (aFolder.flags & FDONOTIFY_FOLDER_FLAG_CHECK_NEW) {
			return true;
		}

		return false;
	},

  /**
   * Put the new mail in a queue ready for notification message.
   */
	handleNewMailReceive : function handleNewMailReceive(rootFolderName, folderName, subject, author) {
		this.logger("MessengerNotifications::handleNewMailReceive(" + rootFolderName + ", " + folderName + ", " + subject + ", " + author + ")");

		if (this.mNotifyTimer) {
			clearTimeout(this.mNotifyTimer);
		}

		if (!this.mailQueue.folderList[rootFolderName]) this.mailQueue.folderList[rootFolderName] = { items: [] };
		var item = { "folderName": folderName, "subject": subject, "author": author };
		this.mailQueue.folderList[rootFolderName].items.push(item);
		// Wait a bit so that we have more message to display
		this.mNotifyTimer = setTimeout("fdonotify.displayNotification()", 5000);
	},

  /**
   * Build the notification message from mails in queue and display
   * it with notify OSD.
   */
	displayNotification : function displayNotification() {
		this.logger("MessengerNotifications::displayNotification");
		try {
			var prefs = Components.classes["@mozilla.org/preferences-service;1"]
			                      .getService(Components.interfaces.nsIPrefBranch);

			var showFolder = prefs.getBoolPref("fdonotify.showFolder");
			var oneNotification = prefs.getBoolPref("fdonotify.oneNotification");
			var persistentNotifications = prefs.getBoolPref("fdonotify.persistentNotifications");
			var index = 0;
			var folderName = "";
			var subject = "";
			var author = "";
			var curFolder = "";
			var body = "";
			var curFolderTotal = 0;
			var totalMail = 0;
			var numFolders = 0;
			for (i in this.mailQueue.folderList) {
				numFolders++;
			}
			for (i in this.mailQueue.folderList) {
				curFolder = i;
				curFolderTotal = this.mailQueue.folderList[i].items.length;
				totalMail += curFolderTotal;
				if (oneNotification && numFolders > 1) {
					body += this.mBundle.formatStringFromName("mail.new.mails.body.oneNotification", [curFolderTotal, "<i>" + curFolder + "<\/i>\r\n"], 2);
				} else {
					index = 0;
					body = "";
					while (index < curFolderTotal) {
						folderName = this.mailQueue.folderList[i].items[index].folderName;
						subject = this.mailQueue.folderList[i].items[index].subject;
						author = this.mailQueue.folderList[i].items[index].author;
						if (index < 2) {
							if (showFolder) {
								body += this.mBundle.formatStringFromName("mail.new.mails.bodyWithFolder", ["<b>" + subject + "<\/b>", author, "<i>" + folderName + "<\/i>"], 3) + "<br>";
							} else {
								body += this.mBundle.formatStringFromName("mail.new.mails.body", ["<b>" + subject + "<\/b>", author], 2) + "<br>";
							}
						}
						index++;
					}
					if (curFolderTotal > 2) body += "+" + (curFolderTotal - 2) + " more\r\n";
					var summary = (totalMail == 1 ? this.mBundle.formatStringFromName("mail.new.mail.summary", [curFolderTotal, curFolder], 2) :
				               this.mBundle.formatStringFromName("mail.new.mails.summary", [curFolderTotal, curFolder], 2));
					this.logger("MessengerNotifications: sending single notification");
					this.sendNotification(summary, body, persistentNotifications);
				}
			}
			if (oneNotification && numFolders > 1) {
				this.logger("MessengerNotifications: sending one notification (and numFolders > 1)");
				var summary = (totalMail == 1 ? this.mBundle.formatStringFromName("mail.new.mail.summary.oneNotification", [totalMail], 1) :
				               this.mBundle.formatStringFromName("mail.new.mails.summary.oneNotification", [totalMail], 1));
				this.sendNotification(summary, body, persistentNotifications);
			}
			this.mailQueue.folderList = [];
			this.mNotifyTimer = null;
		} catch(e) {
			this.mailQueue.folderList = [];
			this.mNotifyTimer = null;
			Components.utils.reportError(e);
			throw e;
		}
	},

  /**
   * Send notification using 'notify-dbus.py' command line.
   */
	sendNotification : function sendNotification(summary, body, isPersistent) {
		try {
			this.logger("MessengerNotifications::sendNotification(" + summary + ", " + body + ", " + isPersistent + ")");
			var file = Components.classes["@mozilla.org/file/local;1"]
			                     .createInstance(Components.interfaces.nsILocalFile);
			file.initWithPath("/usr/bin/env");
			var process = Components.classes["@mozilla.org/process/util;1"]
			                        .createInstance(Components.interfaces.nsIProcess);
			process.init(file);
			var args = ["python", this.mNotifyPath, utf8.encode(summary), utf8.encode(body), "-i", "thunderbird", "-c", "email.arrived", "-a", this.mBundleBrand.GetStringFromName("brandShortName") + Date.now(), "-t", isPersistent ? 0 : -1];
			process.run(false, args, args.length);
		} catch(e) {
			var prompts = Components.classes["@mozilla.org/embedcomp/prompt-service;1"]
			                        .getService(Components.interfaces.nsIPromptService);
			prompts.alert(null, "Dependency error", "To see notifications via D-Bus, the python package needs to be installed.");
			Components.utils.reportError(e);
			throw e;
		}
	}
}

/**
 * Initialise the notification system.
 */
var fdonotify = null;

function fdonotify_onLoad() {
	if (Components.classes["@mozilla.org/xre/app-info;1"].getService(Components.interfaces.nsIXULRuntime).OS == "Linux") {
		fdonotify = new MessengerNotifications();
		removeEventListener("load", fdonotify_onLoad, true);
	}
}

addEventListener("load", fdonotify_onLoad, false);
