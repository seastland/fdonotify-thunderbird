# FDONotify

FDONotify is a simple Thunderbird add-on for Linux to send freedesktop.org notifications for new mail received.

It was originally a fork of [Thunderbird Indicator (libnotify-mozilla)][1]


## Requirements

* Thunderbird for Linux >= 45.0
* Python >= 2.3


## License

Licensed under the GPL v2

See LICENSE for more details


## Installation

Run `build.sh` and install the resulting `fdonotify.xpi` as a Thunderbird Add-on.


[1]: https://launchpad.net/libnotify-mozilla
